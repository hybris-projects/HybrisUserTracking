package trackingapi;

/**
 * This interface represents the customer to be tracked
 */
public interface Customer {

    /**
     * Formats a string describing the customer
     * @return String describing custormer
     */
    String format();

}
