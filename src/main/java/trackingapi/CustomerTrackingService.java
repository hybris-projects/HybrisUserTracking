package trackingapi;

/**
 * This interface applications to track a customer
 */
public interface CustomerTrackingService {

    /**
     * Do some action for tracking the customer
     * @param message describes what the customer did
     */
    void trace(Object message);

}
