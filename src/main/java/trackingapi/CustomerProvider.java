package trackingapi;

/**
 * This interface manages the customers
 */
public interface CustomerProvider {

    /**
     * Get the current customer
     * @return the current customer
     */
    Customer getCustomer();
}
