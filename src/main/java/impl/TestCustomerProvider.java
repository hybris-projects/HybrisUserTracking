package impl;

import trackingapi.Customer;
import trackingapi.CustomerProvider;

/**
 * A CustomerProvider for testing purposes
 */
public class TestCustomerProvider implements CustomerProvider {

    private Customer current;

    public void setCurrent(Customer current) {
        this.current = current;
    }

    @Override
    public Customer getCustomer() {
        return current;
    }
}
