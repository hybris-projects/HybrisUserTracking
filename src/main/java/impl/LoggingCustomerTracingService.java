package impl;

import trackingapi.Customer;
import trackingapi.CustomerProvider;
import trackingapi.CustomerTrackingService;

import java.util.Date;

/**
 * This tracking service class logs messages to System.out
 */
public class LoggingCustomerTracingService implements CustomerTrackingService {

    private CustomerProvider customerProvider;

    public LoggingCustomerTracingService(CustomerProvider customerProvider) {
        this.customerProvider = customerProvider;
    }

    @Override
    public void trace(final Object message) {
        final Customer customer = customerProvider.getCustomer();
        System.out.println(String.format("[%s] [%s] - %s", customer.format(), new Date(), message));
    }

}
