package impl;

import trackingapi.Customer;

/**
 * A Customer for testing purposes
 */
public class TestCustomer implements Customer {

    private String customerName;

    public TestCustomer(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public String format() {
        return customerName;
    }
}
