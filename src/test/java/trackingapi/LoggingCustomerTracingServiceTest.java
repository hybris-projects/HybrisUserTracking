package trackingapi;

import impl.LoggingCustomerTracingService;
import impl.TestCustomer;
import impl.TestCustomerProvider;
import org.testng.annotations.Test;

/**
 * Unit test for the API providing usage examples
 */
public class LoggingCustomerTracingServiceTest {

    private final TestCustomerProvider provider = new TestCustomerProvider();
    private final CustomerTrackingService customerTrackingService = new LoggingCustomerTracingService(provider);

    @Test
    public void tracingTest() {

        provider.setCurrent(new TestCustomer("FOO"));
        // Tracing von FOO
        customerTrackingService.trace("hello world!");
        customerTrackingService.trace("hello world!");
        customerTrackingService.trace("hello world!");
        provider.setCurrent(new TestCustomer("BAR"));
        // Tracing von BAR
        customerTrackingService.trace("hello world!");
        customerTrackingService.trace("hello world!");
        customerTrackingService.trace("hello world!");
    }

}